<?php

namespace app\controllers;

use Carbon\Carbon;
use app\models\Group;
use app\models\Subgroup;
use app\models\Dds;
use app\models\Project;
use app\models\ProjectSearch;
use Yii;

class ReportprojectsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $groups = Group::find()->joinWith('subgroups')->joinWith('articles')->all();

        $dds = Dds::find()->all();

        $carbonDate = Carbon::now();
        $months = array();

        for ($i=0; $i<10; $i++) {
            $months[] = $carbonDate->format('Y-m-01');
            $carbonDate->addMonth();
        }

        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $projects = Project::find()->all();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'groups' => $groups,
            'dds' => $dds,
            'months' => $months,
            'projects' => $projects,
        ]);
    }

}
