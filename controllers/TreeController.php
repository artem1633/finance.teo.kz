<?php

namespace app\controllers;

use app\models\Dds;
use app\models\Article;
use app\models\Group;
use app\models\Subgroup;
use Yii;
use app\models\Tree;
use app\models\TreeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Carbon;

/**
 * TreeController implements the CRUD actions for Tree model.
 */
class TreeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tree models.
     * @return mixed
     */
    public function actionIndex()
    {
//        Tree::deleteAll();
//
//        $groups = Group::find()->all();

//        foreach ($groups as $group) {
//            $tree = new Tree();
//
//            $tree->name = $group->title;
//            $tree->save(false);
//
//            $treeIdGroup = $tree->id;
//
//            $subgroups = Subgroup::findAll(['group_id' => $group->id]);
//
//            $treeId = $tree->id;
//            $groupId = $group->id;
//
//            $groupSumm = 0;
//
//            foreach ($subgroups as $subgroup) {
//                $tree = new Tree();
//
//                $tree->name = $subgroup->title;
//                $tree->tree_id = $treeId;
//
//                $tree->save(false);
//
//                $treeIdSubgroup = $tree->id;
//
//                $articles = Article::find()->where(['group_id' => $groupId])->where(['subgroup_id' => $subgroup->id])->all(); //->where(['=', 'subgroup_id', $tree->id]);
//
//                $subgroupSumm = 0;
//
//                foreach ($articles as $article) {
//                    $tree = new Tree();
//
//                    $tree->name = $article->title;
//                    $tree->tree_id = $treeIdSubgroup;
//
//                    $dds = Dds::find()->where(['article' => $article->id])->one();
//
//                    if ($dds != null) {
//                        if ($dds->sum != null) {
//                            $tree->summ = $dds->sum;
//                            $subgroupSumm += $dds->sum;
//                        }
//                    }
//
//                    $tree->save(false);
//                }
//
//                $subgroupTree = Tree::find()->where(['id' => $treeIdSubgroup])->one();
//                $subgroupTree->summ = $subgroupSumm;
//                $subgroupTree->update(false);
//                $groupSumm += $subgroupSumm;
//            }
//
//            $subgroupTree = Tree::find()->where(['id' => $treeIdGroup])->one();
//            $subgroupTree->summ = $groupSumm;
//            $subgroupTree->update(false);
//
////            return dd($groupId);
//        }

        $searchModel = new TreeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $tree = Tree::find()->all();

        $groups = Group::find()->joinWith('subgroups')->joinWith('articles')->all();

        $dds = Dds::find()->all();

        $carbonDate = Carbon\Carbon::now();
        $months = array();

        for ($i=0; $i<10; $i++) {
            $months[] = $carbonDate->format('Y-m-01');
            $carbonDate->addMonth();
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'groups' => $groups,
            'dds' => $dds,
            'months' => $months,
        ]);
    }

    public function actionAdd()
    {
        $model = new Tree();
        $model->loadDefaultValues();
        $id = Yii::$app->request->get('id');
        $model->tree_id = $id;

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            return $this->redirect(['index']);
        }
        else
        {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays a single Tree model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tree model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tree();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Tree model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Tree model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tree model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tree the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tree::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
