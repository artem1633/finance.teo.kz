<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contragent".
 *
 * @property integer $id
 * @property string $title
 * @property string $type
 *
 * @property Dds[] $dds
 */
class Contragent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contragent';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Наименование',
            'type' => 'Тип',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDds()
    {
        return $this->hasMany(Dds::className(), ['contragent' => 'id']);
    }

    public function getTypes()
    {
        return [
            '2' => 'ПОКУПАТЕЛЬ',
            '1' => 'ПОСТАВЩИК',
        ];
    }
}
