<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report".
 *
 * @property integer $id
 * @property string $ds_left
 * @property string $bdr_project
 * @property string $bdr_mes
 * @property string $dds_fact
 */
class Report extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ds_left', 'bdr_project', 'bdr_mes', 'dds_fact'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ds_left' => 'Остатки ДС',
            'bdr_project' => 'БДР проекты',
            'bdr_mes' => 'БДР мес',
            'dds_fact' => 'ДДС факт',
        ];
    }
}
