<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "crm".
 *
 * @property integer $id
 * @property integer $client
 * @property integer $service
 * @property string $project_sense
 * @property double $pay_all
 * @property double $pay_already
 * @property double $pay_debt
 * @property double $pay_all_worker
 * @property double $pay_already_worker
 * @property double $pay_debt_worker
 * @property string $date_start
 * @property string $date_end_plan
 * @property string $date_end
 * @property string $status
 * @property string $date_status
 * @property string $text
 * @property integer $user_id
 *
 * @property Users $user
 * @property Client $client0
 * @property Service $service0
 */
class Crm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'crm';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client', 'service', 'user_id'], 'integer'],
            [['project_sense', 'text'], 'string'],
            [['pay_all', 'pay_already', 'pay_debt', 'pay_all_worker', 'pay_already_worker', 'pay_debt_worker'], 'number'],
            [['date_start', 'date_end_plan', 'date_end', 'date_status'], 'safe'],
            [['status'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['contragent'], 'exist', 'skipOnError' => true, 'targetClass' => Contragent::className(), 'targetAttribute' => ['client' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client' => 'Клиент',
            'service' => 'Сервис',
            'project_sense' => 'Суть проекта',
            'pay_all' => 'Оплата всего',
            'pay_already' => 'Оплата оплачено',
            'pay_debt' => 'Оплата остаток',
            'pay_all_worker' => 'Сотрудник расчёт всего',
            'pay_already_worker' => 'Сотрудник уже оплачено',
            'pay_debt_worker' => 'Сотрудник долг',
            'date_start' => 'Дата начала работ',
            'date_end_plan' => 'Срок сдачи план',
            'date_end' => 'Срок сдачи факт',
            'status' => 'Статус',
            'date_status' => 'Дата статуса',
            'text' => 'Примечание',
            'user_id' => 'Сотрудник(исполнитель)',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient0()
    {
        return $this->hasOne(Contragent::className(), ['id' => 'client']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService0()
    {
        return $this->hasOne(Service::className(), ['id' => 'service']);
    }

    public function getService()
    {
        return ArrayHelper::map(Service::find()->orderBy('title desc')->asArray()->all(), 'id', 'title');
    }
    public function getUsers()
    {
        return ArrayHelper::map(Users::find()->orderBy('name desc')->asArray()->all(), 'id', 'name');
    }

    public function getClient()
    {
        return ArrayHelper::map(Contragent::find()->where(['type'=>'2'])->orderBy('title desc')->asArray()->all(), 'id', 'title');
    }

    /**
     * @return array
     */
    public function getListStatus()
    {
        return [
            '2' => 'Пауза',
            '1' => 'В работе',
            '3' => 'Сдан',
            '4' => 'Не сдан',
        ];
    }
}
