<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report_dds".
 *
 * @property integer $id
 * @property string $article
 * @property string $date
 * @property double $summ
 * @property string $image
 */
class ReportDds extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_dds';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['summ'], 'number'],
            [['article', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'article' => 'Статья',
            'date' => 'Дата',
            'summ' => 'Сумма',
            'image' => 'Картинка',
        ];
    }
}
