<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "bill".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property integer $juridical
 *
 * @property Juridical $juridical0
 * @property Dds[] $dds
 */
class Bill extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bill';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title', 'text'], 'string'],
            [['juridical'],'integer'],
            [['juridical'], 'exist', 'skipOnError' => true, 'targetClass' => Juridical::className(), 'targetAttribute' => ['juridical' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Наименование',
            'text' => 'Примечание',
            'juridical'=>'Юридическое лицо'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJuridical0()
    {
        return $this->hasOne(Juridical::className(), ['id' => 'juridical']);
    }

    public function getJuridical()
    {
        return ArrayHelper::map(Juridical::find()->orderBy('title desc')->asArray()->all(), 'id', 'title');

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDds()
    {
        return $this->hasMany(Dds::className(), ['bill' => 'id']);
    }
}
