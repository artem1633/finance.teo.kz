<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Dds;

/**
 * DdsSearch represents the model behind the search form about `app\models\Dds`.
 */
class DdsSearch extends Dds
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'contragent', 'article', 'project', 'bill'], 'integer'],
            [['paying_date', 'date_sum', 'type', 'paying_type', 'comment'], 'safe'],
            [['sum'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dds::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'paying_date' => $this->paying_date,
            'sum' => $this->sum,
            'contragent' => $this->contragent,
            'article' => $this->article,
            'project' => $this->project,
            'bill' => $this->bill,
        ]);

        $query->andFilterWhere(['like', 'date_sum', $this->date_sum])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'paying_type', $this->paying_type])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
