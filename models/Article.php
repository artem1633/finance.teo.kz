<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "article".
 *
 * @property integer $id
 * @property integer $group_id
 * @property integer $subgroup_id
 * @property string $title
 * @property string $text
 *
 * @property Subgroup $subgroup
 * @property Group $group
 */
class Article extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'subgroup_id', 'title'], 'required'],
            [['group_id', 'subgroup_id'], 'integer'],
            [['title', 'text'], 'string'],
            [['subgroup_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subgroup::className(), 'targetAttribute' => ['subgroup_id' => 'id']],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => Group::className(), 'targetAttribute' => ['group_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_id' => 'Группа',
            'subgroup_id' => 'Подгруппа',
            'title' => 'Наименование',
            'text' => 'Примечание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubgroup()
    {
        return $this->hasOne(Subgroup::className(), ['id' => 'subgroup_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['id' => 'group_id']);
    }

    public function getDds()
    {
        return $this->hasMany(Dds::className(), ['article' => 'id']);
    }

    public function getListGroup()
    {
        return ArrayHelper::map(Group::find()->orderBy('title desc')->asArray()->all(), 'id', 'title');
    }

    public function getListSubGroup()
    {
        return ArrayHelper::map(Subgroup::find()->orderBy('title desc')->asArray()->all(), 'id', 'title');
    }
}
