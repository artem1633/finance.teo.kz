<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "dds".
 *
 * @property integer $id
 * @property string $paying_date
 * @property string $date_sum
 * @property double $sum
 * @property string $type
 * @property integer $contragent
 * @property integer $article
 * @property integer $project
 * @property string $paying_type
 * @property string $comment
 * @property integer $bill
 *
 * @property Bill $bill0
 * @property Article $article0
 * @property Contragent $contragent0
 * @property Project $project0
 */
class Dds extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dds';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['paying_date'], 'safe'],
            ['paying_date', 'required'],
            [['date_sum', 'comment'], 'string'],
            [['sum'], 'number'],
            [['contragent', 'article', 'project', 'bill'], 'integer'],
            [['type', 'paying_type'], 'string', 'max' => 255],
            [['bill'], 'exist', 'skipOnError' => true, 'targetClass' => Bill::className(), 'targetAttribute' => ['bill' => 'id']],
            [['article'], 'exist', 'skipOnError' => true, 'targetClass' => Article::className(), 'targetAttribute' => ['article' => 'id']],
            [['contragent'], 'exist', 'skipOnError' => true, 'targetClass' => Contragent::className(), 'targetAttribute' => ['contragent' => 'id']],
            [['project'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'paying_date' => 'Дата оплаты',
            'date_sum' => 'ДАТА (ПЕРИОД) НАЧИСЛЕНИЯ (ОТГРУЗКИ)',
            'sum' => 'Сумма',
            'type' => 'Тип',
            'contragent' => 'Контрагент',
            'article' => 'Статья',
            'project' => 'Проект',
            'paying_type' => 'Тип оплаты',
            'comment' => 'Комментарий',
            'bill' => 'Касса',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBill0()
    {
        return $this->hasOne(Bill::className(), ['id' => 'bill']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle0()
    {
        return $this->hasOne(Article::className(), ['id' => 'article']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContragent0()
    {
        return $this->hasOne(Contragent::className(), ['id' => 'contragent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject0()
    {
        return $this->hasOne(Project::className(), ['id' => 'project']);
    }

    public function getContragents()
    {
        return ArrayHelper::map(Contragent::find()->orderBy('title desc')->asArray()->all(), 'id', 'title');
    }

    public function getProject()
    {
        return ArrayHelper::map(Project::find()->orderBy('title desc')->asArray()->all(), 'id', 'title');
    }

    public function getArticle()
    {
        return ArrayHelper::map(Article::find()->orderBy('title desc')->asArray()->all(), 'id', 'title');
    }

    public function getBill()
    {
        return ArrayHelper::map(Bill::find()->orderBy('title desc')->asArray()->all(), 'id', 'title');
    }

    public function getType()
    {
        return [
            '2' => 'С НДС',
            '1' => 'Без НДС',
        ];
    }

    public function getPayType()
    {
        return [
            '2' => 'Нал',
            '1' => 'Безнал',
        ];
    }

}
