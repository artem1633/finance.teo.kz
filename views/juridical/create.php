<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Juridical */

$this->title = 'Добавить ';
$this->params['breadcrumbs'][] = ['label' => 'Юр лица', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="juridical-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
