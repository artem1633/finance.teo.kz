<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Juridical */

$this->title = 'Изменить ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Юр лица', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="juridical-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
