<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Dds */

$this->title = 'Добавить ';
$this->params['breadcrumbs'][] = ['label' => 'Реестр операций', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dds-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
