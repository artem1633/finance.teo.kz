<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Dds */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dds-form">

    <?php $form = ActiveForm::begin([
        'id' => 'order-form',
        'enableAjaxValidation' => true,
        'validationUrl' => ['dds/validate'],
    ]); ?>

		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'paying_date')->input('date') ?>
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'date_sum')->input('date') ?>
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'sum')->textInput() ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
                <?= $form->field($model, 'type')->widget(\kartik\select2\Select2::classname(), [
                    'data' => $model->getType(),
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);	?>
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
                <?= $form->field($model, 'contragent')->widget(\kartik\select2\Select2::classname(), [
                    'data' => $model->getContragents(),
                    'options' => ['placeholder' => 'Контрагент'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);	?>
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
                <?= $form->field($model, 'article')->widget(\kartik\select2\Select2::classname(), [
                    'data' => $model->getArticle(),
                    'options' => ['placeholder' => 'Статья'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);	?>
            </div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
                <?= $form->field($model, 'project')->widget(\kartik\select2\Select2::classname(), [
                    'data' => $model->getProject(),
                    'options' => ['placeholder' => 'Проект'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);	?>
            </div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
                <?= $form->field($model, 'paying_type')->widget(\kartik\select2\Select2::classname(), [
                    'data' => $model->getPayType(),
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);	?>
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
                <?= $form->field($model, 'bill')->widget(\kartik\select2\Select2::classname(), [
                    'data' => $model->getBill(),
                    'options' => ['placeholder' => 'Счёт'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);	?>
            </div>
		</div>
   	
<div style="display:none">
</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>
