<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DdsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dds-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'paying_date') ?>

    <?= $form->field($model, 'date_sum') ?>

    <?= $form->field($model, 'sum') ?>

    <?= $form->field($model, 'type') ?>

    <?php // echo $form->field($model, 'contragent') ?>

    <?php // echo $form->field($model, 'article') ?>

    <?php // echo $form->field($model, 'project') ?>

    <?php // echo $form->field($model, 'paying_type') ?>

    <?php // echo $form->field($model, 'comment') ?>

    <?php // echo $form->field($model, 'bill') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
