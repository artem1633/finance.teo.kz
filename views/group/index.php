<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Группы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-index">
	<div class="box box-default">	
		<div class="box-body">
		<!-- <img src="http://cbscao.ru/sites/default/files/pictures/13249/develop900.jpg" alt="Наш логотип">-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить', ['create'], [
        'data-target'=>'/group/create',
        'class' => 'btn btn-success','onClick'=>"
        $('#modal').modal('show')
        .find('#modal-content')
        .load($(this).attr('data-target'));
        return false;"]) ?>
   </p>
	
		</div>
	</div>
	</div>
	
	<div class="box box-default">	
		<div class="box-body" style="overflow-x: auto;">    
	<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
			//[
			//'attribute'=>'id',
			//'content'=>function ($data){
			//	return '<span class="label label-warning" style="font-size:13px;">'.Html::a($data->id.'   ', ['update','id' =>$data->id], ['style'=>'color:#ffffff;']).'</span>';

			// },
			//],

            // 'id',
            'title:ntext',
			['class' => 'yii\grid\ActionColumn',
			    'template' => '{edit}{delete} ',
                'buttons' => [
                    'edit' => function ($url, $model, $key){
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', [''], ['data-target'=>'/group/update?id='.$key,'onClick'=>"
                            $('#modal').modal('show')
                            .find('#modal-content')
                            .load($(this).attr('data-target'));
                            return false;"]);
                    },
                ],
            ],
           // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

	</div>
</div>
        <?php
    Modal::begin([
        'header' => 'Добавление группы',
        'id' => 'modal',
        'size'=>'modal-lg',
    ]);
    echo "<div id='modal-content'>Загружаю...</div>";
    Modal::end();
    ?>
</div>
