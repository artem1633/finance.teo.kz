<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <div class="box box-default">
    <div class="box-body">
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>
        </div>
         <div class="col-md-2">
            <?= $form->field($model, 'password_readable')->passwordInput(['maxlength' => true]) ?>
         </div>
        <div class="col-md-2">
            <?= $form->field($model, 'role')
                ->textInput()?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'permission')
                ->dropDownList(['manager'=>'Менеджер','admin'=>'Администратор'],
                    ['prompt' => 'Выберите вариант']);	?>
        </div>
         <div class="col-md-2">
            <?= $form->field($model, 'is_access')
                ->dropDownList(['1'=>'ВКЛ','0'=>'ВЫКЛ'],
                ['prompt' => 'Выберите вариант']);	?>
         </div>
     </div>

     <div style="display:none">
         <div class="row">
             <div class="col-md-2">
                <?= $form->field($model, 'created_at')->textInput() ?>
             </div>
         </div>
         <div class="row">
             <div class="col-md-2">
                <?= $form->field($model, 'update_at')->textInput() ?>
             </div>
         </div>
     </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>
</div>
</div>
