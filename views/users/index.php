<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Users;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">
    <?php if($user->permission == Users::PERMISSIONS_ADMIN or $user->permission == Users::PERMISSIONS_VIEW): ?>
        <div class="task-index">
            <div class="box box-default">
                <div class="box-body">

                    <p>
                        <?= Html::a('Добавить', ['create'], [
                            'data-target' => \yii\helpers\Url::base() . '/users/create',
                            'class' => 'btn btn-success']) ?>
                    </p>
                    <?php Pjax::begin(); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="box box-default">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <?php Pjax::begin(); ?>    <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                'id',
                                'name',
                                'permission',
                                'login',
                    //            'password',
                                 'is_deleted:boolean',
                                 'is_access:boolean',
                                 'created_at',
                                // 'update_at',

                                ['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]); ?>
                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
