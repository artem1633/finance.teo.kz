<aside class="main-sidebar">

    <section class="sidebar">

<!--        <!-- Sidebar user panel -->-->
<!--        <div class="user-panel">-->
<!--            <div class="pull-left image">-->
<!--                <img src="--><?//= $directoryAsset ?><!--/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>-->
<!--            </div>-->
<!--            <div class="pull-left info">-->
<!--                <p>Alexander Pierce</p>-->
<!---->
<!--                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>-->
<!--            </div>-->
<!--        </div>-->
<!---->
<!--        <!-- search form -->-->
<!--        <form action="#" method="get" class="sidebar-form">-->
<!--            <div class="input-group">-->
<!--                <input type="text" name="q" class="form-control" placeholder="Search..."/>-->
<!--              <span class="input-group-btn">-->
<!--                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>-->
<!--                </button>-->
<!--              </span>-->
<!--            </div>-->
<!--        </form>-->
<!--        <!-- /.search form -->-->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Отчёты', 'icon' => 'file-code-o', 'url' => ['/tree'],
//                        'items' => [
//                            ['label' => 'ДДС факт', 'icon' => 'file-code-o', 'url' => ['/reportdds']],
//                            ['label' => 'БДР (расходы)', 'icon' => 'file-code-o', 'url' => ['/reportbdrcosts']],
//                        ]
                        'items' => [
                            ['label' => 'ДДС', 'icon' => 'file-code-o', 'url' => ['/tree']],
                            ['label' => 'Проекты', 'icon' => 'file-code-o', 'url' => ['/reportprojects']],
                        ]
                    ],
                    ['label' => 'Пользователи', 'icon' => 'file-code-o', 'url' => ['/users']],
                    ['label' => 'Справочники', 'icon' => 'bookmark', 'url' => ['/'],
                        'items' => [
                            ['label' => 'Счета', 'icon' => 'file-code-o', 'url' => ['/bill']],
                            ['label' => 'Контрагенты', 'icon' => 'file-code-o', 'url' => ['/contragent']],
                            ['label' => 'Юр лица', 'icon' => 'file-code-o', 'url' => ['/juridical']],
                            ['label' => 'Проекты', 'icon' => 'file-code-o', 'url' => ['/project']],
                            ['label' => 'Статьи', 'icon' => 'file-code-o', 'url' => ['/article']],
                            ['label' => 'Услуги', 'icon' => 'file-code-o', 'url' => ['/service']],
                            ['label' => 'Группы', 'icon' => 'file-code-o', 'url' => ['/group']],
                            ['label' => 'Подгруппы', 'icon' => 'file-code-o', 'url' => ['/subgroup']],
                        ],
                    ],
                    ['label' => 'Первичные данные', 'icon' => 'bookmark', 'url' => ['/'],
                        'items' => [
                            ['label' => 'CRM', 'icon' => 'file-code-o', 'url' => ['/crm']],
                            ['label' => 'Реестр операций', 'icon' => 'file-code-o', 'url' => ['/dds']],
                        ],
                    ],



//                    ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
//                    ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
//                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
//                    [
//                        'label' => 'Same tools',
//                        'icon' => 'share',
//                        'url' => '#',
//                        'items' => [
//                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
//                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
//                            [
//                                'label' => 'Level One',
//                                'icon' => 'circle-o',
//                                'url' => '#',
//                                'items' => [
//                                    ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
//                                    [
//                                        'label' => 'Level Two',
//                                        'icon' => 'circle-o',
//                                        'url' => '#',
//                                        'items' => [
//                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
//                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
//                                        ],
//                                    ],
//                                ],
//                            ],
//                        ],
//                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
