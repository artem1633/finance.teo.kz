<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CrmSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="crm-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'client') ?>

    <?= $form->field($model, 'service') ?>

    <?= $form->field($model, 'project_sense') ?>

    <?= $form->field($model, 'pay_all') ?>

    <?php // echo $form->field($model, 'pay_already') ?>

    <?php // echo $form->field($model, 'pay_debt') ?>

    <?php // echo $form->field($model, 'pay_all_worker') ?>

    <?php // echo $form->field($model, 'pay_already_worker') ?>

    <?php // echo $form->field($model, 'pay_debt_worker') ?>

    <?php // echo $form->field($model, 'date_start') ?>

    <?php // echo $form->field($model, 'date_end_plan') ?>

    <?php // echo $form->field($model, 'date_end') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'date_status') ?>

    <?php // echo $form->field($model, 'text') ?>

    <?php // echo $form->field($model, 'user_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
