<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Crm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="crm-form">

    <?php $form = ActiveForm::begin([
        'id' => 'order-form',
        'enableAjaxValidation' => true,
        'validationUrl' => ['crm/validate'],
    ]); ?>

		<div class="row">
			<div class="col-md-4 vcenter">
                <?= $form->field($model, 'client')->widget(\kartik\select2\Select2::classname(), [
                    'data' => $model->getClient(),
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);	?>
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
                <?= $form->field($model, 'service')->widget(\kartik\select2\Select2::classname(), [
                    'data' => $model->getService(),
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);	?>
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'project_sense')->textarea(['rows' => 6]) ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'pay_all')->textInput() ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'pay_already')->textInput() ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'pay_debt')->textInput() ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'pay_all_worker')->textInput() ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'pay_already_worker')->textInput() ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'pay_debt_worker')->textInput() ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'date_start')->input('date') ?>
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'date_end_plan')->input('date') ?>
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'date_end')->input('date') ?>
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
                <?= $form->field($model, 'status')->widget(\kartik\select2\Select2::classname(), [
                    'data' => $model->getListStatus(),
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);	?>
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'date_status')->input('date') ?>
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
                <?= $form->field($model, 'user_id')->widget(\kartik\select2\Select2::classname(), [
                    'data' => $model->getUsers(),
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);	?>
			</div>
		</div>
   	
<div style="display:none">
</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>
