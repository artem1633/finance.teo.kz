<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Crm */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Crms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-view">


    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены что хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'client',
            'service',
            'project_sense:ntext',
            'pay_all',
            'pay_already',
            'pay_debt',
            'pay_all_worker',
            'pay_already_worker',
            'pay_debt_worker',
            'date_start',
            'date_end_plan',
            'date_end',
            'status',
            'date_status',
            'text:ntext',
            'user_id',
        ],
    ]) ?>

</div>
