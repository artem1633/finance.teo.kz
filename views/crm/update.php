<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Crm */

$this->title = 'Изменить ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'CRM', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="crm-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
