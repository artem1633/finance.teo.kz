<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Crm */

$this->title = 'Добавить ';
$this->params['breadcrumbs'][] = ['label' => 'CRM', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
