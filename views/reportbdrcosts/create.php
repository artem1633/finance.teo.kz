<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ReportBdrCosts */

$this->title = 'Добавить БДР (расходы)';
$this->params['breadcrumbs'][] = ['label' => 'БДР (расходы)', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-bdr-costs-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
