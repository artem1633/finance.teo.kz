<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ReportBdrCosts */

$this->title = 'Изменить БДР (расходы): ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'БДР (расходы)', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="report-bdr-costs-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
