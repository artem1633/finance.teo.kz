<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReportBdrCostsSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'БДР (расходы) месяц';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-bdr-costs-index">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="box box-default">
        <div class="box-body" style="overflow-x: auto;">
            <p>
                <?= Html::a('Добавить БДР (расходы)', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
        </div>
    </div>

    <div class="box box-default">
        <div class="box-body" style="overflow-x: auto;">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    'article',
                    'date',
                    'summ',
                    'image',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>
