<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Contragent */

$this->title = 'Добавить ';
$this->params['breadcrumbs'][] = ['label' => 'Контрагенты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contragent-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
