<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Contragent */

$this->title = 'Изменить ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Контрагенты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="contragent-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
