<?php

use yii\helpers\Html;

use leandrogehlen\treegrid\TreeGrid;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TreeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ДДС факт';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="tree-index">


<?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <p>
        <?php
        if ($dataProvider->count == 0)
            echo Html::a('Создать корневой элемент', ['add'], ['class' => 'btn btn-success']);
        ?>
    </p>

    <div class="tree-index">

        </p>


        <section class="content">
            <div class="tree-index">

                </p>

                <div class="tree-index">

                    <div class="box box-default">
                        <div class="box-body" style="overflow-x: auto;">

                            <table id="w0" class="table table-striped table-bordered">

                                <thead>
                                <tr>
                                    <th>
                                        Имя
                                    </th>

                                    <?php
                                    foreach ($months as $month) {
                                        ?>
                                        <th>
                                            <?= $month ?>
                                        </th>
                                        <?php
                                    }
                                    ?>

                                </tr>

                                </thead>
                                <tbody>

                                <?php
                                foreach ($groups as $group) {
                                    ?>

                                    <tr class="treegrid-<?=$group->id?> treegrid-expanded" data-key="<?=$group->id?>">
                                        <td>
                                        <span class="treegrid-expander treegrid-expander-expanded">

                                        </span>
                                            <?= $group->title ?>
                                        </td>


                                        <?php

                                        foreach ($months as $month) {
                                            $carbonMonth = \Carbon\Carbon::createFromFormat('Y-m-d', $month);

                                            $groupSumm = 0;

                                            foreach ($group->subgroups as $subgroup) {
                                                $articles = \app\models\Article::find()->where(['subgroup_id' => $subgroup->id])->all();

                                                foreach ($articles as $article) {
                                                    foreach ($dds as $d) {
                                                        $carbonPayingdate = \Carbon\Carbon::createFromFormat('Y-m-d', $d->paying_date);
                                                        if ($d->article == $article->id && $carbonMonth->month == $carbonPayingdate->month) {
                                                            $groupSumm += $d->sum;
                                                        }
                                                        ?>

                                                        <?php
                                                    }
                                                }
                                            }

                                            echo('<td>' . $groupSumm . ' </td>');
                                        }
                                        ?>

                                    </tr>
                                    <?php
                                    foreach ($group->subgroups as $subgroup) {
                                        ?>

                                        <tr class="treegrid-<?=$subgroup->id?> treegrid-parent-<?=$group->id?>" data-key="<?=$subgroup->id?>">
                                            <td>


                                                </span>
                                                <span class="treegrid-expander">

                                        </span>
                                                <?= $subgroup->title ?>
                                            </td>



                                            <?php
                                            $articles = \app\models\Article::find()->where(['subgroup_id' => $subgroup->id])->all();

                                            foreach ($months as $month) {
                                                $carbonMonth = \Carbon\Carbon::createFromFormat('Y-m-d', $month);

                                                $supgroupSumm = 0;

                                                foreach ($articles as $article) {
                                                    foreach ($dds as $d) {
                                                        $carbonPayingdate = \Carbon\Carbon::createFromFormat('Y-m-d', $d->paying_date);
                                                        if ($d->article == $article->id && $carbonMonth->month == $carbonPayingdate->month) {
                                                            $supgroupSumm += $d->sum;
                                                        }
                                                        ?>

                                                        <?php
                                                    }
                                                }

                                                echo('<td>' . $supgroupSumm . ' </td>');
                                            }
                                            ?>

                                            <!--                                         ДЛЯ СЛЕДУЮЩИХ ЯЧЕЕК, В СТОЛБЦАХ ОБОЗНАЧАЮЩИХ МЕСЯЦ - ПОДГРУППЫ   -->

                                        </tr>

                                        <?php

                                        $articles = \app\models\Article::find()->where(['subgroup_id' => $subgroup->id])->all();
                                        foreach ($articles as $article) {
                                            ?>
                                            <tr class="treegrid-<?=$article->id?> treegrid-parent-<?=$subgroup->id?>" data-key="<?=$article->id?>">
                                                <td>
                                        <span class="treegrid-indent">

                                        </span>
                                                    <span class="treegrid-indent">

                                        </span>
                                                    <span class="treegrid-expander">

                                        </span>

                                                    <?= $article->title ?>
                                                </td>

                                                <?php
                                                foreach ($months as $month) {
                                                    $carbonMonth = \Carbon\Carbon::createFromFormat('Y-m-d', $month);

                                                    $articleSumm = 0;
                                                    foreach ($dds as $d) {
                                                        $carbonPayingdate = \Carbon\Carbon::createFromFormat('Y-m-d', $d->paying_date);
                                                        if ($d->article == $article->id && $carbonMonth->month == $carbonPayingdate->month) {
                                                            $articleSumm = $d->sum;
                                                        }
                                                        ?>

                                                        <!--                                               ДЛЯ СЛЕДУЮЩИХ ЯЧЕЕК СУММЫ, В СТОЛБЦАХ ОБОЗНАЧАЮЩИХ МЕСЯЦ - СТАТЬИ -->

                                                        <?php
                                                    }

                                                    echo('<td>' . $articleSumm . ' </td>');
                                                }
                                                ?>

                                            </tr>

                                            <?php
                                        }
                                        ?>

                                        <?php
                                    }
                                    ?>

                                    <?php
                                }
                                ?>


                                </tbody>

                            </table>
                        </div>
                    </div>



                </div>
            </div>
        </section>


    </div>

    <div class="box box-default" style="display:none">
        <div class="box-body" style="overflow-x: auto;">
            <?=
            TreeGrid::widget([
                'dataProvider' => $dataProvider,
                'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                'keyColumnName' => 'id',
                'showOnEmpty' => FALSE,
                'parentColumnName' => 'tree_id',
                'columns' => [

                ]
            ]);
            ?>

        </div>
