<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ReportDds */

$this->title = 'Изменить ДДС факт: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'ДДС факт', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="report-dds-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
