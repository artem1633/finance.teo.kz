<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Subgroup */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subgroup-form">

    <?php $form = ActiveForm::begin([
        'id' => 'order-form',
        'enableAjaxValidation' => true,
        'validationUrl' => ['subgroup/validate'],
    ]); ?>

		<div class="row">
			<div class="col-md-4 vcenter">
                <?= $form->field($model, 'group_id')->widget(\kartik\select2\Select2::classname(), [
                    'data' => $model->getListGroup(),
                    'options' => ['placeholder' => 'Группа'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);	?>
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'title')->textInput() ?>
			</div>
		</div>
   	
<div style="display:none">
</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>
