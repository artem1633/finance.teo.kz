<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Subgroup */

$this->title = 'Изменить ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Подгруппы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="subgroup-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
