<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Subgroup */

$this->title = 'Добавить ';
$this->params['breadcrumbs'][] = ['label' => 'Подгруппы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subgroup-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
