<?php

use yii\helpers\Html;

use leandrogehlen\treegrid\TreeGrid;

/* @var $this yii\web\View */
$this->title = 'Бдр проекты';
$this->params['breadcrumbs'][] = $this->title;
?>



<div class="tree-index">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



    <div class="tree-index">

        </p>


        <section class="content">
            <div class="tree-index">

                </p>

                <div class="tree-index">

                    <div class="box box-default">
                        <div class="box-body" style="overflow-x: auto;">

                            <table id="w0" class="table table-striped table-bordered">

                                <thead>
                                <tr>
                                    <th>
                                        Имя
                                    </th>

                                    <th>
                                        Итого
                                    </th>

                                    <?php
                                    foreach ($projects as $project) {
                                        ?>
                                        <th>
                                            <?= $project->title ?>
                                        </th>
                                        <?php
                                    }
                                    ?>

                                </tr>

                                </thead>
                                <tbody>

                                <?php
                                foreach ($groups as $group) {
                                    ?>

                                    <tr class="treegrid-<?=$group->id?> treegrid-expanded" data-key="<?=$group->id?>">
                                        <td>
                                        <span class="treegrid-expander treegrid-expander-expanded">

                                        </span>
                                            <?= $group->title ?>
                                        </td>

                                        <td>

                                        </td>

                                        <?php

                                        foreach ($projects as $project) {

                                            $groupSumm = 0;

                                            $articles = \app\models\Article::find()->where(['subgroup_id' => $groups[1]->subgroups[0]->id])->all();

                                            foreach ($group->subgroups as $subgroup) {
                                                $articles = \app\models\Article::find()->where(['subgroup_id' => $subgroup->id])->all();

                                                foreach ($articles as $article) {
                                                    foreach ($dds as $d) {

                                                        if ($d->article == $article->id && $project->id == $d->project) {
                                                            $groupSumm += $d->sum;
                                                        }
                                                        ?>

                                                        <?php
                                                    }
                                                }
                                            }

                                            echo('<td>' . $groupSumm . ' </td>');
                                        }
                                        ?>

                                    </tr>
                                    <?php
                                    foreach ($group->subgroups as $subgroup) {
                                        ?>

                                        <tr class="treegrid-<?=$subgroup->id?> treegrid-parent-<?=$group->id?>" data-key="<?=$subgroup->id?>">
                                            <td>

                                                </span>
                                                <span class="treegrid-expander">

                                        </span>
                                                <?= $subgroup->title ?>
                                            </td>

                                            <td>

                                            </td>

                                            <?php

                                            $articles = \app\models\Article::find()->where(['subgroup_id' => $subgroup->id])->all();

                                            foreach ($projects as $project) {

                                                $supgroupSumm = 0;

                                                foreach ($articles as $article) {
                                                    foreach ($dds as $d) {
                                                        if ($d->article == $article->id && $project->id == $d->project) {
                                                            $supgroupSumm += $d->sum;
                                                        }
                                                        ?>

                                                        <?php
                                                    }
                                                }

                                                echo('<td>' . $supgroupSumm . ' </td>');
                                            }
                                            ?>

                                            <!--                                         ДЛЯ СЛЕДУЮЩИХ ЯЧЕЕК, В СТОЛБЦАХ ОБОЗНАЧАЮЩИХ МЕСЯЦ - ПОДГРУППЫ   -->

                                        </tr>

                                        <?php

                                        $articles = \app\models\Article::find()->where(['subgroup_id' => $subgroup->id])->all();
                                        foreach ($articles as $article) {
                                            ?>
                                            <tr class="treegrid-<?=$article->id?> treegrid-parent-<?=$subgroup->id?>" data-key="<?=$article->id?>">
                                                <td>
                                        <span class="treegrid-indent">

                                        </span>
                                                    <span class="treegrid-indent">

                                        </span>
                                                    <span class="treegrid-expander">

                                        </span>

                                                    <?= $article->title ?>
                                                </td>


                                                <?php
                                                $summByArticle = 0;
                                                foreach ($projects as $project) {
                                                    $ddsByArticleAndProject = \app\models\Dds::find()->where(['project' => $project->id, 'article' => $article->id])->all();

                                                    foreach ($ddsByArticleAndProject as $d) {
                                                        $summByArticle += $d->sum;
                                                    }

                                                }
                                                ?>


                                                <td>
                                                    <?= $summByArticle ?>
                                                </td>

                                                <?php
                                                foreach ($projects as $project) {

                                                    $articleSumm = 0;
                                                    foreach ($dds as $d) {
                                                        if ($d->article == $article->id && $project->id == $d->project) {
                                                            $articleSumm = $d->sum;
                                                        }
                                                        ?>

                                                        <!--                                               ДЛЯ СЛЕДУЮЩИХ ЯЧЕЕК СУММЫ, В СТОЛБЦАХ ОБОЗНАЧАЮЩИХ МЕСЯЦ - СТАТЬИ -->

                                                        <?php
                                                    }

                                                    echo('<td>' . $articleSumm . ' </td>');
                                                }
                                                ?>

                                            </tr>

                                            <?php
                                        }
                                        ?>

                                        <?php
                                    }
                                    ?>

                                    <?php
                                }
                                ?>


                                </tbody>

                            </table>
                        </div>
                    </div>



                </div>
            </div>
        </section>


    </div>

    <div class="box box-default" style="display:none">
        <div class="box-body" style="overflow-x: auto;">
            <?=
            TreeGrid::widget([
                'dataProvider' => $dataProvider,
                'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                'keyColumnName' => 'id',
                'showOnEmpty' => FALSE,
                'parentColumnName' => 'id',
                'columns' => [

                ]
            ]);
            ?>

        </div>

