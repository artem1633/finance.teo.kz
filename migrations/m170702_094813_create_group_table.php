<?php

use yii\db\Migration;

/**
 * Handles the creation of table `group`.
 */
class m170702_094813_create_group_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('group', [
            'id' => $this->primaryKey(),
            'title'=>$this->string()->notNull(),
        ]);

        $this->createIndex('idx-article-group_id', 'article', 'group_id', false);
        $this->addForeignKey("fk-article-group_id", "article", "group_id", "group", "id", 'CASCADE');

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey('fk-article-group_id','article');
        $this->dropIndex('idx-article-group_id','article');

        $this->dropTable('group');
    }
}
