<?php

use yii\db\Migration;

/**
 * Handles the creation of table `report`.
 */
class m170702_105606_create_report_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('report', [
            'id' => $this->primaryKey(),
            'ds_left'=>$this->text(),
            'bdr_project'=>$this->text(),
            'bdr_mes'=>$this->text(),
            'dds_fact'=>$this->text(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('report');
    }
}
