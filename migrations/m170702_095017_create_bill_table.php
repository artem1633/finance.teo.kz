<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bill`.
 */
class m170702_095017_create_bill_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('bill', [
            'id' => $this->primaryKey(),
            'title'=>$this->string()->notNull(),
            'text'=>$this->text(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('bill');
    }
}
