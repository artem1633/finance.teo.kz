<?php

use yii\db\Migration;

/**
 * Handles the creation of table `crm`.
 */
class m170702_104549_create_crm_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('crm', [
            'id' => $this->primaryKey(),
            'client'=>$this->integer(),
            'service'=>$this->integer(),
            'project_sense'=>$this->text(),
            'pay_all'=>$this->float(),
            'pay_already'=>$this->float(),
            'pay_debt'=>$this->float(),
            'pay_all_worker'=>$this->float(),
            'pay_already_worker'=>$this->float(),
            'pay_debt_worker'=>$this->float(),
            'date_start'=>$this->date(),
            'date_end_plan'=>$this->date(),
            'date_end'=>$this->date(),
            'status'=>$this->string(),
            'date_status'=>$this->date(),
            'text'=>$this->text(),
            'user_id'=>$this->integer(),
        ]);

        $this->createIndex('idx-crm-client', 'crm', 'client', false);
        $this->addForeignKey("fk-crm-client", "crm", "client", "client", "id", 'CASCADE');
        $this->createIndex('idx-crm-users', 'crm', 'user_id', false);
        $this->addForeignKey("fk-crm-user-id", "crm", "user_id", "users", "id", 'CASCADE');

        $this->createIndex('idx-crm-service', 'crm', 'service', false);
        $this->addForeignKey("fk-crm-service", "crm", "service", "service", "id", 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey("fk-crm-client", "crm");
        $this->dropIndex('idx-crm-client', 'crm');
        $this->dropForeignKey("fk-crm-user-id", "crm");

        $this->dropIndex('idx-crm-users', 'crm');
        $this->dropForeignKey("fk-crm-service", "crm");
        $this->dropIndex('idx-crm-service', 'crm');


        $this->dropTable('crm');
    }
}
