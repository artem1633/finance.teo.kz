<?php

use yii\db\Migration;

/**
 * Handles the creation of table `subgroup`.
 */
class m170702_094819_create_subgroup_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('subgroup', [
            'id' => $this->primaryKey(),
            'group_id'=>$this->integer()->notNull(),
            'title'=>$this->string(),

        ]);

        $this->createIndex('idx-article-subgroup_id', 'article', 'subgroup_id', false);
        $this->addForeignKey("fk-article-subgroup_id", "article", "subgroup_id", "subgroup", "id", 'CASCADE');
        $this->createIndex('idx-group-subgroup_id', 'subgroup', 'group_id', false);
        $this->addForeignKey("fk-group-subgroup_id", "subgroup", "group_id", "group", "id", 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey("fk-group-subgroup_id", "subgroup");
        $this->dropIndex('idx-group-subgroup_id', 'subgroup');
        $this->dropForeignKey('fk-article-subgroup_id','article');
        $this->dropIndex('idx-article-subgroup_id','article');


        $this->dropTable('subgroup');
    }
}
