<?php

use yii\db\Migration;

class m170717_135249_create_table_bdr_costs extends Migration
{
    public function safeUp()
    {
        $this->createTable('report_bdr_costs', [
            'id' => $this->primaryKey(),
            'article'=>$this->string(),
            'date'=>$this->date(),
            'summ'=>$this->float(),
            'image'=>$this->string(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('report_bdr_costs');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170717_135249_create_table_bdr_costs cannot be reverted.\n";

        return false;
    }
    */
}
