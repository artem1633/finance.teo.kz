<?php

use yii\db\Migration;

class m170717_131521_create_table_report_dds extends Migration
{
    public function safeUp()
    {
        $this->createTable('report_dds', [
            'id' => $this->primaryKey(),
            'article'=>$this->string(),
            'date'=>$this->date(),
            'summ'=>$this->float(),
            'image'=>$this->string(),
        ]);

    }

    public function safeDown()
    {
        $this->dropTable('report_dds');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170717_131521_create_table_report_dds cannot be reverted.\n";

        return false;
    }
    */
}
