<?php

use yii\db\Migration;

class m170717_141059_create_table_report_bdr_projects extends Migration
{
    public function safeUp()
    {
        $this->createTable('report_bdr_projects', [
            'id' => $this->primaryKey(),
            'article'=>$this->string(),
            'projects'=>$this->string(),
            'summ'=>$this->float(),
            'total'=>$this->float(),
            'image'=>$this->string(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('report_bdr_projects');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170717_141059_create_table_report_bdr_projects cannot be reverted.\n";

        return false;
    }
    */
}
