<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m170501_085638_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql')
        {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('users', [
            'id' => $this->primaryKey() . ' NOT NULL AUTO_INCREMENT',
            'name' => $this->string()->notNull()->unique(),
            'permission' => $this->string()->notNull(),
            'role'=>$this->text()->notNull(),
            'login' => $this->string()->notNull()->unique(),
            'password' => $this->string(),
            'is_deleted' => $this->boolean()->defaultValue(1),
            'is_access' => $this->boolean()->defaultValue(1),
            'created_at' => $this->dateTime(),
            'update_at' => $this->dateTime(),
            'password_readable'=>$this->string(),
        ], $tableOptions);

        $this->insert('users',array(
            'name' => 'Administrator',
            'permission' => 'admin',
            'role'=>'admin',
            'login' => 'admin',
            'password' => md5('admin'),
            'is_deleted'=>false,
            'is_access'=>true,
            'created_at'=>date ("Y-m-d H:i:s", time()),
            'update_at'=>date ("Y-m-d H:i:s", time()),
        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users');
    }
}
