<?php

use yii\db\Migration;

class m170706_181014_add_props_to_jur extends Migration
{
    public function up()
    {
        $this->dropColumn('juridical','comment');
        $this->addColumn('juridical','comment','text');
    }
}
