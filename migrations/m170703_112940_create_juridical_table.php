<?php

use yii\db\Migration;

/**
 * Handles the creation of table `juridical`.
 */
class m170703_112940_create_juridical_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('juridical', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'comment' => $this->text(),
        ]);

        $this->addColumn('bill', 'juridical', 'integer');
        $this->addColumn('contragent', 'type', 'string');

        $this->createIndex('idx-bill-juridical', 'bill', 'juridical', false);
        $this->addForeignKey("fk-bill-juridical", "bill", "juridical", "juridical", "id", 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey("fk-bill-juridical", "bill");
        $this->dropIndex('idx-bill-juridical', 'bill');

        $this->dropColumn('bill', 'juridical');
        $this->dropColumn('contragent', 'type');
        $this->dropTable('juridical');
    }
}
