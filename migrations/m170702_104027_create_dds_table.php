<?php

use yii\db\Migration;

/**
 * Handles the creation of table `dds`.
 */
class m170702_104027_create_dds_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('dds', [
            'id' => $this->primaryKey(),
            'paying_date' => $this->date(),
            'date_sum'=>$this->text(),
            'sum'=>$this->float(),
            'type'=>$this->string(),
            'contragent'=>$this->integer(),
            'article'=>$this->integer(),
            'project'=>$this->integer(),
            'paying_type'=>$this->string(),
            'comment'=>$this->text(),
            'bill'=>$this->integer(),
        ]);
        $this->createIndex('idx-dds-contragent', 'dds', 'contragent', false);
        $this->addForeignKey("fk-dds-contragent", "dds", "contragent", "contragent", "id", 'CASCADE');
        $this->createIndex('idx-dds-article', 'dds', 'article', false);
        $this->addForeignKey("fk-dds-article", "dds", "article", "article", "id", 'CASCADE');
        $this->createIndex('idx-dds-project', 'dds', 'project', false);
        $this->addForeignKey("fk-dds-project", "dds", "project", "project", "id", 'CASCADE');
        $this->createIndex('idx-dds-bill', 'dds', 'bill', false);
        $this->addForeignKey("fk-dds-bill", "dds", "bill", "bill", "id", 'CASCADE');

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey("fk-dds-contragent", "dds");
        $this->dropIndex('idx-dds-contragent', 'dds');
        $this->dropForeignKey("fk-dds-article", "dds");
        $this->dropIndex('idx-dds-article', 'dds');
        $this->dropForeignKey("fk-dds-project", "dds");
        $this->dropIndex('idx-dds-project', 'dds');
        $this->dropForeignKey("fk-dds-bill", "dds");
        $this->dropIndex('idx-dds-bill', 'dds');

        $this->dropTable('dds');
    }
}
