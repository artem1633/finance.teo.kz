<?php

use yii\db\Migration;

class m170724_103834_create_table_tree extends Migration
{
    public function safeUp()
    {
        $this->createTable('tree', [
            'id' => $this->primaryKey(),
            'name'=>$this->string(),
            'summ'=>$this->string(),
            'tree_id'=>$this->integer(),
        ]);

        $this->addCommentOnColumn('tree','name','имя');
        $this->addCommentOnColumn('tree','summ','сумма');
        $this->addCommentOnColumn('tree','tree_id','предок');

//        $this->addForeignKey("tree", "{{%ranks}}", "level", "{{%user}}", "level", 'RESTRICT');
        $this->addForeignKey('fk-tree-parent', 'tree', 'tree_id', 'tree', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('tree');

        return false;
    }

//CREATE TABLE `tree` (
//`id` INTEGER(11) NOT NULL AUTO_INCREMENT,
//`name` VARCHAR(255) COLLATE utf8_general_ci NOT NULL COMMENT 'имя',
//`url` VARCHAR(255) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'путь',
//`tree_id` INTEGER(11) DEFAULT NULL COMMENT 'предок',
//PRIMARY KEY (`id`) USING BTREE,
//UNIQUE KEY `id` (`id`) USING BTREE,
//UNIQUE KEY `name` (`name`) USING BTREE,
//UNIQUE KEY `url` (`url`) USING BTREE,
//KEY `tree_id` (`tree_id`) USING BTREE,
//CONSTRAINT `treee_fk1` FOREIGN KEY (`tree_id`) REFERENCES `tree` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
//) ENGINE=InnoDB
//AUTO_INCREMENT=14 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
//;



    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170724_103834_create_table_tree cannot be reverted.\n";

        return false;
    }
    */
}
