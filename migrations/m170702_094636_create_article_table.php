<?php

use yii\db\Migration;

/**
 * Handles the creation of table `article`.
 */
class m170702_094636_create_article_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('article', [
            'id' => $this->primaryKey(),
            'group_id'=>$this->integer()->notNull(),
            'subgroup_id'=>$this->integer()->notNull(),
            'title'=>$this->string()->notNull(),
            'text'=>$this->text(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('article');
    }
}
