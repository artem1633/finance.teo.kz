<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contragent`.
 */
class m170702_103920_create_contragent_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('contragent', [
            'id' => $this->primaryKey(),
            'title'=>$this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('contragent');
    }
}
