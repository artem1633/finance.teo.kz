<?php
namespace app\filters;

use Yii;
use yii\base\ActionFilter;
use yii\web\ForbiddenHttpException;

class BanFilter extends ActionFilter
{
    const BAN_TEXT = 'Ошибка доступа';

    public function beforeAction($action)
    {
        if(Yii::$app->user->isGuest == false){
            $user = Yii::$app->user->identity;
            $userEnable = $user->is_access;
            if($userEnable == 0)
                throw new ForbiddenHttpException(self::BAN_TEXT);
        }
        return parent::beforeAction($action);
    }

    public function afterAction($action, $result)
    {
        return parent::afterAction($action, $result);
    }
}
?>